"""Launch file"""
import threading

import updater


def setup():
    """Creates update bot instances and adds sidebar threads"""
    updaters = []

    # ~~~~~~~~~~~~~~~~
    # r/DestinyTheGame
    dtg_updater = updater.Updater("DestinyTheGame", "dtgbot")

    # Daily
    dtg_updater.add_sidebar_thread("Daily", "DQ", "Daily Questions {3}", "DTG_Bot", "Daily Questions")
    dtg_updater.add_sidebar_thread("Daily", "DRT", "[D2] Daily Reset Thread {3}", "DTG_Bot", "[D2] Daily Reset")
    # Monday
    dtg_updater.add_sidebar_thread("Monday", "FF", "Focused Feedback", "DTG_Bot", "Focused Feedback")
    # Tuesday
    dtg_updater.add_sidebar_thread("Tuesday", "TUT", "Daily Thread - Team Up Tuesday!", "DTG_Bot", "Team Up Tuesday")
    dtg_updater.add_sidebar_thread("Tuesday", "LH", "Weekly Loot Hub", "DTG_Bot", "Weekly Loot Hub")
    dtg_updater.add_sidebar_thread("Tuesday", "WRT", "[D2] Weekly Reset Thread {3}", "DTG_Bot", "[D2] Weekly Reset")
    dtg_updater.add_sidebar_thread("Tuesday", "IB", "[D2] Iron Banner Megathread {3}", "DTG_Bot", "[D2] Iron Banner")
    # dtg_updater.addSidebarThread('Tuesday', 'WVR', 'Weekly Vendor Reset Megathread {3}', 'DTG_Bot')
    # Wednesday
    dtg_updater.add_sidebar_thread("Wednesday", "RW", "DAILY THREAD - RANT WEDNESDAY", "DTG_Bot", "Rant Wednesday")
    # dtg_updater.addSidebarThread('Wednesday', 'ARM', 'Armsday Rolls, week {2}', 'wileykyoto')
    # Thursday
    dtg_updater.add_sidebar_thread("Thursday", "LT", "Daily Thread - Lore Thursday", "DTG_Bot", "Lore Thursday")
    dtg_updater.add_sidebar_thread("Thursday", "TWAB", "This Week In Destiny", "DTG_Bot", "This Week At Bungie")
    # Friday
    dtg_updater.add_sidebar_thread(
        "Friday", "FTF", "Daily Discussion - Free Talk Friday!", "DTG_Bot", "Free Talk Friday"
    )
    dtg_updater.add_sidebar_thread("Friday", "XUR", "[D2] Xûr Megathread", "DTG_Bot", "[D2] Xûr")
    dtg_updater.add_sidebar_thread(
        "Friday", "TOO", "[D2] Trials of Osiris Megathread", "DTG_Bot", "[D2]Trials of Osiris"
    )
    # Saturday
    dtg_updater.add_sidebar_thread("Saturday", "SFS", "Salt-Free Saturday", "DTG_Bot", "Salt-Free Saturday")
    # Sunday
    # dtg_updater.add_sidebar_thread(
    #     "Sunday", "WIH", "This Week in DestinyTheGame History", "DTG_Bot", "This Week in History"
    # )
    dtg_updater.add_sidebar_thread("Sunday", "SPL", "Sunday Plz", "DTG_Bot", "Sunday Plz")

    # Append the updater
    updaters.append(dtg_updater)

    # ~~~~~~~~~~~~
    # r/FortniteBR
    fortnitebr_updater = updater.Updater("FortNiteBR", "fortnitebr_repliedbot")

    # Daily
    # None
    # Monday
    fortnitebr_updater.add_sidebar_thread("Monday", "MM", "Mentor Monday {4}", "AutoModerator")
    # Tuesday
    fortnitebr_updater.add_sidebar_thread("Tuesday", "FTT", "Free Talk Tuesday {4}", "AutoModerator")
    # Wednesday
    fortnitebr_updater.add_sidebar_thread("Wednesday", "EPW", "Epic Plz Wednesday {4}", "AutoModerator")
    # Thursday
    fortnitebr_updater.add_sidebar_thread("Thursday", "TIUT", "Think It Up Thursday {4}", "AutoModerator")
    # Friday
    fortnitebr_updater.add_sidebar_thread("Friday", "TUF", "Team Up Friday {4}", "AutoModerator")
    # Saturday
    fortnitebr_updater.add_sidebar_thread("Saturday", "FF", "Focused Feedback - ", "AutoModerator")
    # Sunday
    fortnitebr_updater.add_sidebar_thread("Sunday", "SALT", "Salty Sunday {4}", "AutoModerator")

    # https://www.reddit.com/r/FortNiteBR/comments/akncb4/mentor_monday_28jan2019/
    # https://www.reddit.com/r/FortNiteBR/comments/andz6z/free_talk_tuesday_05feb2019/
    # https://www.reddit.com/r/FortNiteBR/comments/anqlfe/epic_plz_wednesday_06feb2019/
    # https://www.reddit.com/r/FortNiteBR/comments/ao3lnn/think_it_up_thursday_07feb2019/
    # https://www.reddit.com/r/FortNiteBR/comments/aogfad/team_up_friday_08feb2019/
    # https://www.reddit.com/r/FortNiteBR/comments/aos7r5/focused_feedback_zombies/
    # https://www.reddit.com/r/FortNiteBR/comments/ampfwi/salty_sunday_03feb2019/

    # Append the updater
    updaters.append(fortnitebr_updater)

    return updaters


def main():
    """
    Main function

    Runs the setup to create updater bots, and then launches each within its own thread
    """
    updaters = setup()
    for updater_bot in updaters:
        threading.Thread(target=updater_bot.launch).start()
