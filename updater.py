"""Reddit Sidebar Updater Bot"""

from time import strftime, sleep
from datetime import datetime, timedelta
import configparser
import logging
import logging.config
from typing import Iterator
from retrying import retry
from layer7_utilities import oAuth, LoggerConfig
import praw
import praw.models
from _version import __version__

config = configparser.ConfigParser()
config.read("botconfig.ini")

__botname__ = "SidebarUpdater"
__description__ = "Updates subreddit sidebar"
__author__ = "u/D0cR3d"
__dsn__ = config.get("BotConfig", "DSN")
__logpath__ = config.get("BotConfig", "logpath")

_dbusername = config.get("Database", "USERNAME")
_dbpassword = config.get("Database", "PASSWORD")
_dbhost = config.get("Database", "HOST")


class SidebarUpdater:
    """Sidebar Update Bot"""

    def __init__(
        self,
        r: praw.Reddit,
        logger: logging.Logger,
        day: str,
        tag: str,
        subreddit_name: str,
        thread_title: str,
        author_name: str,
        newreddit_linkname: str,
    ):
        self.r = r
        self.start_tag = "[](#" + tag + "S)"
        self.end_tag = "[](#" + tag + "E)"
        self.subreddit: praw.models.Subreddit = self.r.subreddit(subreddit_name)
        self.subreddit_search = self.subreddit
        self.day = day
        self._thread_title_unformatted = thread_title
        self.author_name = author_name
        self.newreddit_linkname = newreddit_linkname

        self.logger = logger

    @retry(stop_max_attempt_number=6, wait_fixed=300000)
    def side_bar_updater(self):
        """
        The actual sidebar update function.

        Works by performing a search using the Reddit API, and updating the sidebar link if found. Works with Old
        Reddit and New Reddit, thanks to TT.
        """
        # Syntax: https://www.reddit.com/wiki/search/#wiki_reddit_search
        # When making a multi-word field search, wrap the query in double or single quotes. For example,
        # title:“kitten gif”.
        # time_filter should be "day" for normal operation, since the updater instances run per-day or daily
        results: Iterator[praw.models.Submission] = self.subreddit.search(
            f'title:"{self.thread_title}" author:"{self.author_name}"', time_filter="day", sort="new"
        )

        if not results:
            return

        self.logger.debug("Updating thread - " + self.thread_title)

        for submissions in results:
            if str(submissions.author).lower() != str(self.author_name).lower():
                # self.logger.debug("Thread {} not by {}".format(submissions.fullname, self.author_name))
                continue
            submissionurl = f"https://redd.it/{submissions.id}"

            # Update new Reddit
            widgets = self.subreddit.widgets
            widgets.refresh()
            topbar = widgets.topbar
            if len(self.newreddit_linkname) > 0 and len(topbar) > 0:
                topbar = topbar[0]
                try:
                    assert isinstance(topbar, praw.models.Menu)
                    update = False
                    for menu in topbar:
                        if not isinstance(menu, praw.models.Submenu):
                            if menu.text != self.newreddit_linkname or menu.url == submissionurl:
                                continue
                            menu.url = submissionurl
                            update = True
                        else:
                            for child in menu:
                                if child.text != self.newreddit_linkname or child.url == submissionurl:
                                    continue
                                child.url = submissionurl
                                update = True
                    if update:
                        topbar.mod.update(data=list(topbar))
                        self.subreddit.widgets.refresh()
                        widgets.refresh()
                        self.logger.info("New Reddit menu updated with %s Thread", self.thread_title)
                except AssertionError:
                    self.logger.debug("no topbar")
                except Exception as e:
                    self.logger.exception("Failure in side_bar_updater()")
                    raise e

            # Update old Reddit
            content = self.subreddit.wiki["config/sidebar"].content_md
            text = f"[{submissions.title}]({submissionurl})"
            try:
                start = content.index(self.start_tag)
                end = content.index(self.end_tag) + len(self.end_tag)
                self.logger.debug("Old text - %s\nNew text - %s", content[start:end], text)
                if content[start:end] != f"{self.start_tag}{text}{self.end_tag}":
                    content = content.replace(content[start:end], f"{self.start_tag}{text}{self.end_tag}")

                    try:
                        self.logger.debug("Attempting to update sidebar")
                        self.subreddit.wiki["config/sidebar"].edit(content, reason="Updating Sidebar Thread Link")
                        self.logger.info("Sidebar Updated with %s Thread", self.thread_title)
                        sleep(90)  # this is important
                    except Exception as e:
                        self.logger.warning("Unable to edit wiki/update sidebar: %s", e)
                        raise e
                else:
                    self.logger.debug(self.thread_title + " - Nothing changed, skipping update")
            except ValueError:
                self.logger.warning("Substring not found: %s to %s", self.start_tag, self.end_tag)
            except Exception as e:
                self.logger.exception("Failure in side_bar_updater()")
                raise e

    @property
    def curtime(self) -> str:
        """
        Returns current hour

        %H Hour (24-hour clock) as a decimal number [00,23]
        """
        return strftime("%H")

    @property
    def sbdate(self) -> str:
        """
        Returns current month and day in the following format:

        `MM-DD`

        Example: `08-17`

        %m Month as a decimal number [01,12]
        %d Day of the month as a decimal number [01,31]
        """
        return strftime("%m-%d")

    @property
    def fortnite_date(self) -> str:
        """
        Returns current day in the following format:

        `DD/<short month>/YYYY`

        Example: 17/Aug/2022

        %d Day of the month as a decimal number [01,31]
        %b Locale's abbreviated month name
        %Y Year with century as a decimal number
        """
        return strftime("%d/%b/%Y")

    @property
    def today_tomorrow(self) -> str:
        """
        Returns current day and following day in the following format:

        `MM/DD - MM/DD`

        %m Month as a decimal number [01,12]
        %d Day of the month as a decimal number [01,31]

        Example: `08/17 - 08/18`
        """
        return strftime("%m/%d") + " - " + strftime("%m/%d", (datetime.now() + timedelta(days=1)).timetuple())

    @property
    def thread_title(self) -> str:
        """
        Return formatted thread title


        The strftime string that is substituted in depends on what is passed in the thread title. These are properties
        in the class, so check them for documentation.

        {0} returns sbdate
        {1} returns today_tomorrow
        {2} returns armsday_week
        {3} returns weekly_thread
        {4} returns fortnite_date

        """
        return self._thread_title_unformatted.format(
            self.sbdate, self.today_tomorrow, self.armsday_week, self.weekly_thread, self.fortnite_date
        )

    @property
    def armsday_week(self) -> str:
        """Returns weeks since Arms Day along with the current day in the following format:

        `<weeks> [YYYY-MM-DD]`

        Example: `362 [2022-08-17]`
        """
        armsday_start = datetime(2015, 9, 2)
        return str((datetime.today() - armsday_start).days // 7) + strftime(" [%Y-%m-%d]")

    @property
    def weekly_thread(self) -> str:
        """
        Returns the current day in the following format:

        `[YYYY-MM-DD]`

        Example: `[2022-08-17]`
        """
        return strftime("[%Y-%m-%d]")


class Updater:
    """Updater bot, which runs instances of SidebarUpdater"""

    def __init__(self, subreddit: str, agent: str):
        self.days = {
            "Monday": [],
            "Tuesday": [],
            "Wednesday": [],
            "Thursday": [],
            "Friday": [],
            "Saturday": [],
            "Sunday": [],
            "Daily": [],
        }
        self.subreddit_name = subreddit
        self.subreddit_search = subreddit
        self.agent = agent
        self.r = None

        #################### LOGGER SETUP ############################
        # Setup logging
        loggerconfig = LoggerConfig(__dsn__, __botname__, __version__, __logpath__)
        logging.config.dictConfig(loggerconfig.get_config())
        self.logger = logging.getLogger("root")
        self.logger.info("/*********Starting App*********\\")
        self.logger.info("App Name: %s | Version: %s", __botname__, __version__)
        ################################################################

        self.can_launch = self.login()

    def login(self) -> bool:
        """Login to the database to retrieve bot credentials"""
        try:
            self.logger.info("Attempting to get accounts for agent `%s`", self.agent)
            auth = oAuth()
            auth.get_accounts(
                self.agent,
                __description__,
                __version__,
                __author__,
                __botname__,
                _dbusername,
                _dbpassword,
                _dbhost,
                "TheTraveler",
            )
            for account in auth.accounts:
                self.r = account.login()
                break
            self.logger.info("Connected to account: %s", self.r.user.me())
            return True
        except AttributeError:
            self.logger.critical("Failed to login. No account found")
            return False
        except Exception:  # pylint: disable=broad-except
            self.logger.error("Failed to log in.")
            return False

    def add_sidebar_thread(self, day: str, tag: str, thread_title: str, author_name: str, newreddit_linkname: str = ""):
        """Add a new sidebar link to be updated"""
        if not self.can_launch:
            return
        new_thread = SidebarUpdater(
            self.r, self.logger, day, tag, self.subreddit_name, thread_title, author_name, newreddit_linkname
        )
        self.days[day].append(new_thread)

    def run_updater(self):
        """
        Run the updates

        For every threaded instance matching today's date and daily threads, run the update function
        """
        today = strftime("%A")
        self.logger.debug("Running threads for %s", today)
        for thread in self.days[today] + self.days["Daily"]:
            try:
                thread.side_bar_updater()
            except ValueError:
                # Daily post hasn't been put up yet, not a problem
                self.logger.info("Daily thread not posted yet - No problems.")
            except Exception:  # pylint: disable=broad-except
                self.logger.exception("Error updating sidebar")
        self.logger.debug("Sleeping for 2 minutes.")
        sleep(120)

    def launch(self):
        """Launcher

        If the bot is able to launch, runs the updater which then handles the individual SidebarUpdater instances
        """
        while self.can_launch:
            self.run_updater()
        sleep(600)
